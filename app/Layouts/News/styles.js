import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../../Utils/Colors';

var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#fff',
  },
  newsImg: {
    width: deviceWidth,
    height: 300,
   resizeMode: 'cover'
  },
  newsTitle: {
    fontFamily: 'bariol-regular',
    fontSize: 22,
    color: '#000'
  },
  newsDate: {
    fontFamily: 'bariol-regular',
    color: Colors.buttonGray,
    fontSize: 16,
    paddingTop: 10,
  },
  newsBody: {
    fontFamily: 'bariol-regular',
    fontSize: 18,
    paddingTop: 10,
    paddingBottom: 10,
    lineHeight: 30,
    color: '#000'
  }
});