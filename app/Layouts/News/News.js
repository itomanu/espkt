import React, { Component } from 'react';
import { StyleSheet, View, Text, ScrollView, Image } from 'react-native';
import { Content, Body } from 'native-base';
import Colors from '../../Utils/Colors';
import { Voca } from '../../Utils';
import { styles } from './styles';

export default class News extends Component {
  render() {
    return (
      <Content style={{ backgroundColor: '#fff' }}>
        <Image style={styles.newsImg} source={{ uri: this.props.img }} />
        <Content style={styles.container}>
          <Text style={styles.newsTitle}>{Voca.titleCase(this.props.title)}</Text>
          <Text style={styles.newsDate}>26 Maret 2018</Text>
          <Text style={styles.newsBody}>{Voca.capitalize(this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body + this.props.body)}</Text>
        </Content>
      </Content>
    )
  }
};