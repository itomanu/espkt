import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../../Utils/Colors';

var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  list: {
    marginRight: 18,
    borderBottomWidth: 0
  },
  profilItemField: {
    color: Colors.headerColorG2,
    paddingBottom: 10
  },
  profilItemData: {
  },
  gradientHeader: {
    position: 'absolute',
    width: deviceWidth,
    height: 300
  },
});