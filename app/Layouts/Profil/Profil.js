import React, { Component } from 'react';
import { View } from 'react-native'
import { Content, List, ListItem, Body, Text, Thumbnail } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';

import { BaseComp, BaseContent, GradientHeader, FooterMenu } from '../../Components';
import { Colors } from '../../Utils';

import { styles } from './styles';

export default class Profil extends Component {
  render() {
    return (
      <BaseComp>
        <LinearGradient start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}} colors={[Colors.headerColorG1, Colors.headerColorG2]} style={styles.gradientHeader} />
        <GradientHeader title='Profil'/>
        <Content>
          <View style={{
            justifyContent: 'center',
            alignItems: 'center',
            padding: 10
          }}>
            <Thumbnail large source={{uri: 'https://www.wowkeren.com/images/news/00134440.jpg'}} style={{width: 100, height: 100, marginBottom: 15, borderRadius: 100}}/>
            <Text style={{fontSize: 22, color: '#fff'}}>Bunga Citra Lestari</Text>
          </View>
          <BaseContent>
            <List>
              <ProfilItem field='NIK' text='31002307200230004'/>
              <ProfilItem field='Alamat' text='Jalan KDP Slamet No. 2, Bandar Lor, Mojoroto, Kota Kediri, Jawa Timur 64114, Indonesia'/>
              <ProfilItem field='HP' text='08157402000'/>
              <ProfilItem field='Email' text='bcl2002@yahoo.co.id'/>
              <ProfilItem field='Pekerjaan' text='Mahasiswi'/>
            </List>
          </BaseContent>
        </Content>
      </BaseComp>
    )
  }
};

class ProfilItem extends Component {
  render() {
    return (
      <ListItem style={styles.list}>
        <Body>
          <Text style={styles.profilItemField}>{this.props.field}</Text>
          <Text >{this.props.text}</Text>
        </Body>
      </ListItem>
    )
  }
};
