import React, { Component } from 'react';
import { View } from 'react-native';
import { Button, Text } from 'native-base';
import { Router } from '../../Utils';
import { registerAppListener, registerKilledListener } from '../../Utils/Listeners';
import FCM from 'react-native-fcm';

registerKilledListener();

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = { token: '' };
  }
  
  async componentDidMount() {
    registerAppListener(this.props.navigation);
    FCM.getInitialNotification().then(notif => {
      this.setState({
        initNotif: notif
      });
      if (notif && notif.targetScreen == 'detail') {
        setTimeout(() => {
          this.props.navigation.navigate('Detail')
        }, 500);
      }
    });

    try {
      let result = await FCM.requestPermissions({ badge: false, sound: true, alert: false });
    } catch(e) {
      console.error(e);
    }

    FCM.getFCMToken()
      .then(token => {
        console.log('TOKEN: ', token);
        this.setState({token: token || ''})
      })
      .catch((error) => {
        console.error();
      });
  }

  render() {
    return (
      <View>
        <Button onPress={() => {
          Router.navigateTo()
        }}>
          <Text>Main</Text>
        </Button>
        <Button onPress={() => {
          FCM.getFCMToken()
          .then(token => {
            console.log('TOKEN: ', token);
            this.setState({token: token || ''})
          })
          .catch((error) => {
            console.error();
          });
        }}><Text>get Token</Text></Button>
        <Text>Token: </Text>
        <Text>{this.state.token}</Text>
      </View>
    )
  }
};
