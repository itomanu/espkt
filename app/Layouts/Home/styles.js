import { StyleSheet, Dimensions } from 'react-native';

var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  gradientHeader: {
    position: 'absolute',
    width: deviceWidth,
    height: 150
  },
  content: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
  },
  newsTitle: {
    margin: 10,
    fontSize: 20
  },
  listItem: {
    paddingTop: 0,
    paddingRight: 0,
    paddingLeft: 0,
    marginTop: 0,
    marginLeft: 0,
    marginRight: 10,
  },
  card: {
    borderRadius: 8,
    width: 180,
    height: 120,
  },
  cardItem: {
    width: 180,
    height: 120,
    borderRadius: 8,
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    paddingRight: 0
  },
  gradientImg: {
    borderRadius: 8,
    width: 180,
    height: 120,
    position: 'absolute'
  },
  cardTitleBox: {
    position: 'absolute',
    width: 180,
    height: 120,
    alignItems: 'flex-start',
    padding: 10
  },
  cardTitle: {
    color: '#fff',
    fontSize: 20,
    lineHeight: 30,
  },
  cardImg: {
    borderRadius: 8,
    width: 180,
    height: 120,
    padding: 0,
    opacity: .1
  }
});