import React, { Component } from 'react';
import { Image, Alert, ActivityIndicator } from 'react-native'
import { Content, View, Text, List, ListItem, Card, CardItem, Body } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { BaseComp, GradientHeader, FooterMenu, HomeHeader, HomeMenu } from '../../Components';
import { Colors, Constants, Voca, Router } from '../../Utils';
import { styles } from './styles';
import Axios from 'axios';

const dummyImg = 'https://suaraaceh.com/wp-content/uploads/2017/11/IMG-20171123-WA0015.jpg';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: true };
  }

  componentDidMount() {
    Axios.get(Constants.network.NEWS_URL)
      .then((res) => {
        this.setState({
          isLoading: false,
          news: res.data
        });
      })
      .catch((error) => {
        console.error(error);
      })
  }

  renderNews() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator />
        </View>
      );
    } else {
      return (
        <List style={{ paddingLeft: 10 }} contentContainerStyle={{ paddingRight: 10 }}
          horizontal={true} showsHorizontalScrollIndicator={false} pagingEnabled={true}
          decelerationRate={'normal'} snapToInterval={195} snapToAlignment={'start'}
          dataArray={this.state.news}
          renderRow={(item, section, index) =>
            <ListItem style={styles.listItem}>
              <NewsItem index={index} title={Voca.titleCase(item.title)} img={dummyImg} onPress={() => this._onPress(item.title, dummyImg, item.body)} />
            </ListItem>
          }
        />
      );
    }
  }

  _onPress(title, img, body) {
    this.props.navigator.push({
      screen: 'news',
      title: '',
      passProps: {
        title: title,
        img: img,
        body: body
      },
      navigatorStyle: {
        topBarElevationShadowEnabled: false,
        navBarHideOnScroll: true,
        navBarBackgroundColor: 'transparent',
        drawUnderNavBar: true,
        navBarTranslucent: true,
        navBarButtonColor: '#ffffff',
        navBarTextColor: '#ffffff',
        tabBarHidden: true,
      }
    });
  }

  render() {
    return (
      <BaseComp>
        <LinearGradient start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }} colors={[Colors.headerColorG1, Colors.headerColorG2]} style={styles.gradientHeader} />
        <HomeHeader />
        <Content>
          <HomeMenu />
          <View>
            <Text style={styles.newsTitle}>
              {'Berita Humas Polres'.toUpperCase()}
            </Text>
            {this.renderNews()}
          </View>
        </Content>
      </BaseComp>
    )
  }
};

class NewsItem extends Component {
  constructor(props) {
    super(props);
  }

  getOverlayGradient(index) {
    return Colors.newsOverlayGradient[this.props.index % Colors.newsOverlayGradient.length];
  }

  render() {
    return (
      <Card style={styles.card}>
        <CardItem style={styles.cardItem} activeOpacity={.8} button onPress={this.props.onPress}>
          <Body>
            <LinearGradient start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }} colors={this.getOverlayGradient()} style={styles.gradientImg} />
            <Image style={styles.cardImg} source={{ uri: this.props.img }} />
            <View style={styles.cardTitleBox}>
              <Text numberOfLines={4} ellipsizeMode='tail' style={styles.cardTitle}>{this.props.title}</Text>
            </View>
          </Body>
        </CardItem>
      </Card>
    )
  }
};

