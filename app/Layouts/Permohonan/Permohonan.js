import React, { Component } from 'react';
import { Content } from 'native-base';
import { BaseComp, GradientHeader, FooterMenu } from '../../Components';

export default class Permohonan extends Component {
  render() {
    return (
      <BaseComp>
        <GradientHeader title='permohonan'/>
        <Content />
      </BaseComp>
    )
  }
};
