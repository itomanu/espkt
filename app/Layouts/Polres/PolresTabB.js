import React, { Component } from 'react';
import { Image } from 'react-native'
import { Content, Text } from 'native-base';
import { styles } from './styles';
import { Constants } from '../../Utils';

export default class PolresTabA extends Component {
  render() {
    return (
      <Content style={{padding: 20}}>
        <Text style={styles.polresText}>
          {Constants.about}
        </Text>
      </Content>
    )
  }
};
