import React, { Component } from 'react';
import { Image, View } from 'react-native'
import { Content, Text } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { styles } from './styles';
import { Constants } from '../../Utils';

export default class PolresTabA extends Component {
  render() {
    return (
      <Content padder>
        <Grid style={styles.polresGrid}>
          <PolresItem
            src={require('../../../assets/img/ic-pol-address.png')}
            text={Constants.address} />
          <PolresItem
            src={require('../../../assets/img/ic-pol-phone.png')}
            text={Constants.phoneNumber} />
          <PolresItem
            src={require('../../../assets/img/ic-pol-email.png')}
            text={Constants.email} />
          <PolresItem
            src={require('../../../assets/img/ic-pol-web.png')}
            text={Constants.web} />
          <Row/>
        </Grid>
      </Content>
    )
  }
};

class PolresItem extends Component {
  render() {
    return (
      <Row style={styles.polresItemRow}>
        <Col size={20} style={{alignItems: 'center', justifyContent: 'center'}}>
            <Image style={styles.polresItemImg}
              source={this.props.src}/>
        </Col>
        <Col size={80} style={{
          justifyContent: 'center'
        }}>
          <Text>{this.props.text}</Text>
        </Col>
      </Row>
    )
  }
};
