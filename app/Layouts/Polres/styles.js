import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../../Utils/Colors';

var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  tabContainer: {
    elevation: 0
  },
  tabBarUnderline: {
    height: 2,
  },
  tabStyle: {
    borderBottomWidth: 2,
    borderBottomColor: Colors.backgroundGray,
  },
  tabText: {
    fontSize: 18,
  },
  tabActiveText: {
    fontSize: 18,
    fontWeight: '300'
  },
  polresGrid: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.backgroundGray,
    paddingBottom: 10
  },
  polresItemRow: {
    paddingBottom: 10,
    paddingTop: 10
  },
  polresItemImg: {
    width: 30,
    height: 30,
    tintColor: Colors.defaultBlue
  },
  polresText: {
    lineHeight: 25
  }
});