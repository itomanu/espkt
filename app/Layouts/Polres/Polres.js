import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Content, Tab, Tabs, Text } from 'native-base';
import { BaseComp, BaseContent, GradientHeader, FooterMenu } from '../../Components';

import PolresTabA from './PolresTabA';
import PolresTabB from './PolresTabB';

import { styles } from './styles';

export default class Polres extends Component {
  render() {
    return (
      <BaseComp>
        <GradientHeader title='Polres Kediri Kota'/>
        <BaseContent>
          <Tabs tabContainerStyle={styles.tabContainer} tabBarUnderlineStyle={styles.tabBarUnderline}>
            <Tab heading="Hubungi Kami" tabStyle={styles.tabStyle} textStyle={styles.tabText} activeTextStyle={styles.tabActiveText}>
              <PolresTabA />
            </Tab>
            <Tab heading="Tentang Polres" tabStyle={styles.tabStyle} textStyle={styles.tabText} activeTextStyle={styles.tabActiveText}>
              <PolresTabB />
            </Tab>
          </Tabs>
        </BaseContent>
      </BaseComp>
    )
  }
};
