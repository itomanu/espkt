import React, { Component } from 'react';
import { View, ActivityIndicator, Alert } from 'react-native';
import { Content, List, ListItem, Body, Text } from 'native-base';
import { BaseComp, GradientHeader, FooterMenu, BaseContent } from '../../Components';
import { styles } from './styles';
import { Constants, Voca } from '../../Utils';
import Axios from 'axios';

export default class Inbox extends Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: true };
  }

  componentDidMount() {
    Axios.get(Constants.network.INBOX_URL)
      .then((res) => {
        this.setState({
          isLoading: false,
          inbox: res.data
        }, () => {});
      })
      .catch((error) => {
        console.error(error);
      })
  }

  renderInbox() {
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, padding: 20, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator/>
        </View>
      );
    } else {
      return (
        <List contentContainerStyle={{paddingBottom: 30}} dataArray={this.state.inbox} renderRow={(item) => {
          return (
            <ListItem style={styles.list} onPress={() => {
              Alert.alert(Voca.titleCase(item.title), Voca.capitalize(item.body));
            }}>
              <InboxItem body={Voca.titleCase(item.title)} date='24 Maret 2018 - 10.15 WIB' />
            </ListItem>
          )}} />
      );
    }
  }

  render() {
    return (
      <BaseComp>
        <GradientHeader title='Inbox'/>
        <BaseContent>
          {this.renderInbox()}
        </BaseContent>
      </BaseComp>
    )
  }
};

class InboxItem extends Component {
  render() {
    return (
      <Body>
        <Text style={styles.inboxItemText}>{this.props.body}</Text>
        <Text style={styles.inboxItemDate}>{this.props.date}</Text>
      </Body>
    )
  }
};
