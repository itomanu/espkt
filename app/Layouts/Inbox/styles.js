import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../../Utils/Colors';

var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  list: {
    marginRight: 18
  },
  inboxItemText: {
    paddingBottom: 10
  },
  inboxItemDate: {
    color: Colors.buttonGray
  }
});