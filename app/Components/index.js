import BaseComp from './BaseComp';
import BaseContent from './BaseContent';
import GradientHeader from './GradientHeader';
import HomeHeader from './HomeHeader';
import HomeMenu from './HomeMenu';

export { BaseComp, BaseContent, GradientHeader, HomeHeader, HomeMenu }