import React, { Component } from 'react';
import { StyleProvider, Container, Content } from 'native-base';

import getTheme from './../../native-base-theme/components';
import material from './../../native-base-theme/variables/material';
import { Colors } from '../Utils';

export default class BaseComp extends Component {
  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <Container style={{backgroundColor: Colors.backgroundGray}}>
          {this.props.children}
        </Container>
      </StyleProvider>
    )
  }
};
