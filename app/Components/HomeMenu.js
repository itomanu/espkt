import React, { Component } from 'react';
import { StyleSheet, Image, Alert } from 'react-native';
import { Card, CardItem, Body, Text, Icon } from 'native-base';

import { Col, Row, Grid } from 'react-native-easy-grid';
import { Constants } from '../Utils';

const colSize = 3;

export default class HomeMenu extends Component {
  createMenu() {
    const rowSize = Math.round(Constants.homeMenu.length / colSize); 
    let menu = [];

    for (let i = 0; i < rowSize; i++) {
      let item = [];
      
      for (let j = 0; j < colSize; j++) {
        let key = (i * colSize) + j;
        if (key < Constants.homeMenu.length)
          item.push(<Col key={key}><MenuItem title={Constants.homeMenu[key].label} icon={Constants.homeMenu[key].icon}/></Col>);
      }
      
      menu.push(<Row key={i}>{item}</Row>)
    }

    return menu;
  }

  render() {
    return (
      <Grid style={styles.grid}>{this.createMenu()}</Grid>
    )
  }
};

class MenuItem extends Component {
  render() {
    return (
      <Card style={styles.card}>
        <CardItem style={styles.cardItem} activeOpacity={.5} button onPress={() => {
          Alert.alert(this.props.title)
        }}>
          <Body>
            <Image style={{width: 25, height: 25}} source={this.props.icon}/>
          </Body>
          <Body style={{justifyContent: 'flex-end'}}>
            <Text style={styles.cardText}>{this.props.title}</Text>
          </Body>
        </CardItem>
      </Card>
    );
  }
};

const styles = StyleSheet.create({
  grid: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
  },
  card: {
    borderRadius: 8
  },
  cardItem: {
    borderRadius: 8,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    height: 100,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  cardText: {
    textAlign: 'left',
    fontSize: 14
  }
});