import React, { Component } from 'react';
import { StyleSheet, Dimensions, StatusBar, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Header, Left, Right, Button, Icon, Body, Title, Thumbnail } from 'native-base';

import variables from './../../native-base-theme/variables/platform';

import { Colors } from '../Utils';

var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

export default class GradientHeader extends Component {
  render() {

    return (
      <View>
        <LinearGradient start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}} colors={[Colors.headerColorG1, Colors.headerColorG2]} style={styles.gradientHeader} />
        <Header noShadow style={styles.header} {...this.props} androidStatusBarColor='#000'>
          <Left style={{flex:1}}>
            <Thumbnail source={{uri: 'https://www.wowkeren.com/images/news/00134440.jpg'}} style={{width: 50, height: 50}}/>
          </Left>
          <Body style={{flex:3}}/>
          <Right style={{flex:1, justifyContent: 'center', alignItems: 'flex-end'}}>
            <Title style={{textAlign: 'right'}}>eSPKT</Title>
          </Right>
        </Header>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'transparent',
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 20
  },
  gradientHeader: {
    position: 'absolute',
    width: deviceWidth,
    height:
      variables.platform === "ios" && variables.platformStyle === "material"
        ? variables.toolbarHeight + StatusBar.height
        : variables.toolbarHeight
  },
});