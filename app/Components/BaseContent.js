import React, { Component } from 'react';
import { StyleSheet } from 'react-native'
import { Content } from 'native-base'

export default class BaseContent extends Component {
  render() {
    return (
      <Content style={styles.content}>
        {this.props.children}
      </Content>
    )
  }
};

const styles = StyleSheet.create({
  content: {
    backgroundColor: '#fff',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    paddingTop: 10
  }
});
