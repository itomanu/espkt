import React, { Component } from 'react';
import { StyleSheet, Dimensions, StatusBar, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Header, Left, Right, Button, Icon, Body, Title } from 'native-base';

import variables from './../../native-base-theme/variables/platform';

import { Colors } from '../Utils';

var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

export default class GradientHeader extends Component {
  render() {
    const butt = this.props.displayBackArrow ? <Button transparent><Icon name='ios-arrow-back'/></Button> : <View />

    return (
      <View>
        <LinearGradient start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}} colors={[Colors.headerColorG1, Colors.headerColorG2]} style={styles.gradientHeader} />
        <Header noShadow style={styles.header} {...this.props} androidStatusBarColor='#000'>
          <Left style={{flex:1}}>
            {butt}
          </Left>
          <Body style={{flex: 3, justifyContent: 'center', alignItems: 'center',}}>
            <Title>{this.props.title.toUpperCase()}</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'transparent'
  },
  gradientHeader: {
    position: 'absolute',
    width: deviceWidth,
    height:
      variables.platform === "ios" && variables.platformStyle === "material"
        ? variables.toolbarHeight + StatusBar.height
        : variables.toolbarHeight
  },
});