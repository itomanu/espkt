import { Navigation } from 'react-native-navigation';

import Login from '../Layouts/Login/Login';
import Home from '../Layouts/Home/Home';
import Polres from '../Layouts/Polres/Polres';
import Permohonan from '../Layouts/Permohonan/Permohonan';
import Inbox from '../Layouts/Inbox/Inbox';
import Profil from '../Layouts/Profil/Profil';
import News from '../Layouts/News/News';
import { Constants, Colors } from '../Utils';

export const Router = {
  register() {
    Navigation.registerComponent('login', () => Login);
    Navigation.registerComponent('home', () => Home);
    Navigation.registerComponent('polres', () => Polres);
    Navigation.registerComponent('permohonan', () => Permohonan);
    Navigation.registerComponent('inbox', () => Inbox);
    Navigation.registerComponent('profil', () => Profil);
    Navigation.registerComponent('news', () => News);
  },

  navigateTo(type, screenApp, props) {
    var passProps = {};
    if (props != null) {
      passProps = props;
    }
  
    if (type == "single") {
      screenApp.passProps = passProps;
      Navigation.startSingleScreenApp(screenApp);
    } else {
      Navigation.startTabBasedApp({
        tabs: Constants.mainMenu,
        appStyle: {
          tabFontFamily: 'bariol-regular',
          forceTitlesDisplay: true,
          navBarBackgroundColor: Colors.headerColorG2,
          tabBarButtonColor: Colors.buttonGray,
          tabBarSelectedButtonColor: Colors.headerColorG2
        },
        passProps: passProps,
        animationType: 'fade'
      });
    }
  }
}