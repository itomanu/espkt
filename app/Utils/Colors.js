const pattern = ['#65cae7', '#f16a78', '#6bc099', '#7972b4', '#df89b6', '#f7a592', '#56c5cc', '#bca0cc', '#6d8bc7'];

const Colors = {
  pattern: pattern,
  headerColorG1: '#728bc3',
  headerColorG2: '#eb6c7b',
  defaultBlue: '#7972b4',
  buttonGray: '#aaa',
  backgroundGray: '#e6e6e6',
  newsOverlayGradient: [
    [pattern[1], pattern[7]],
    [pattern[3], pattern[2]],
    [pattern[0], pattern[8]],
    [pattern[4], pattern[2]],
  ]
};

export default Colors;