const BASE_URL = 'https://jsonplaceholder.typicode.com';

export const Constants = {
  network: {
    BASE_URL: BASE_URL,
    NEWS_URL: BASE_URL+'/posts',
    INBOX_URL: BASE_URL+'/posts'
  },
  address: 'Jalan KDP Slamet No. 2, Bandar Lor, Mojoroto, Kota Kediri, Jawa Timur 64114, Indonesia',
  phoneNumber: '0354-600374',
  email: 'humasreskedirikota@gmail.com',
  web: 'polreskedirikota.com',
  about: 'Berdiri sejak tahun 2012, Gedung Polres Kediri Kota sebelumnya merupakan gedung yang digunakan oleh Kepolisian Wilayah Ex Karesidenan Kediri. Seiring dengan pemekaran wilayah Kabupaten Kediri dan peleburan Kepolisian Wilayah Ex Karesidenan Kediri, didirikan Kepolisian Resor Kediri Kota yang terpisah dengan Kepolisian Resor Kediri Kabupaten.',
  mainMenu: [
    {
      label: 'Home',
      screen: 'home',
      icon: require('./../../assets/img/ic-nav-home.png'),
      selectedIcon: require('./../../assets/img/ic-nav-home.png'),
      titleImage: require('./../../assets/img/ic-nav-home.png'),
      navigatorStyle: {
        navBarHidden: true
      },
    },
    {
      label: 'Polres',
      screen: 'polres',
      icon: require('./../../assets/img/ic-nav-polres.png'),
      selectedIcon: require('./../../assets/img/ic-nav-polres.png'),
      titleImage: require('./../../assets/img/ic-nav-polres.png'),
      navigatorStyle: {
        navBarHidden: true
      },
    },
    {
      label: 'Permohonan',
      screen: 'permohonan',
      icon: require('./../../assets/img/ic-nav-permohonan.png'),
      selectedIcon: require('./../../assets/img/ic-nav-permohonan.png'),
      titleImage: require('./../../assets/img/ic-nav-permohonan.png'),
      navigatorStyle: {
        navBarHidden: true
      },
    },
    {
      label: 'Inbox',
      screen: 'inbox',
      icon: require('./../../assets/img/ic-nav-inbox.png'),
      selectedIcon: require('./../../assets/img/ic-nav-inbox.png'),
      titleImage: require('./../../assets/img/ic-nav-inbox.png'),
      navigatorStyle: {
        navBarHidden: true
      },
    },
    {
      label: 'Profil',
      screen: 'profil',
      icon: require('./../../assets/img/ic-nav-profil.png'),
      selectedIcon: require('./../../assets/img/ic-nav-profil.png'),
      titleImage: require('./../../assets/img/ic-nav-profil.png'),
      navigatorStyle: {
        navBarHidden: true
      },
    },
  ],
  homeMenu: [
    {
      label: 'Laporan Pengaduan',
      icon: require('./../../assets/img/ic-menu-pengaduan.png')
    },
    {
      label: 'Laporan Kehilangan',
      icon: require('./../../assets/img/ic-menu-kehilangan.png')
    },
    {
      label: 'Permohonan Pembuatan SIM',
      icon: require('./../../assets/img/ic-menu-sim.png')
    },
    {
      label: 'Pembuatan SKCK',
      icon: require('./../../assets/img/ic-menu-skck.png')
    },
    {
      label: 'Perijinan Keramaian',
      icon: require('./../../assets/img/ic-menu-keramaian.png')
    },
    {
      label: 'Perijinan Lalu Lintas',
      icon: require('./../../assets/img/ic-menu-lalin.png')
    },
    {
      label: 'Surat Keterangan Bebas Narkoba',
      icon: require('./../../assets/img/ic-menu-narkoba.png')
    },
    {
      label: 'Perijinan Senpi Handak',
      icon: require('./../../assets/img/ic-menu-senpi.png')
    },
    {
      label: 'Konsultasi Petugas SPKT',
      icon: require('./../../assets/img/ic-menu-konsul.png')
    },
  ],
};