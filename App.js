/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { Router } from './app/Utils';

Router.register();

Navigation.startSingleScreenApp({
  screen: 
    {
      label: 'Login',
      screen: 'login',
      title: 'Login',
      navigatorStyle:{
        navBarHidden:true
      }
    }
});